#include "gtest/gtest.h"       //reference Googletest
#include "../src/someFile.cpp" // load own Code we want to test.

TEST(SomeTestSuite, SomeTest)
{ // Definition of SomeTest, which belongs to a suite.
  MyType *obj = new MyType();
  obj->set_the_value(5);

  ASSERT_EQ(obj->get_the_value(), 5);
}